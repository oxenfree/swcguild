function checkValid()	{
//check for at least one lettter to the name field

		function nameValidate() {
			var name = document.getElementById("nameInput").value;
			
			if (name.length == 0) {
			showError("Name is required", "errorName")
			}
    
 		}
//check for at least one digit to the phone field

    	function emailPhoneValidate()	{
			var email = document.getElementById("emailInput").value;
			var phone = document.getElementById("phoneInput").value;
      
			if (email.length == 0 && phone.length ==0) {
			showError("Email or phone is required", "errorEmailPhone")
			}
      }
//check for a brief message if "other" in reason for inquiry is chosen

		function reasonForInquiry()	{
    
			var select = document.getElementById("reasonSelect").value;
			var message = document.getElementById("briefMessage").value;
      
			if (select == "other" && message == 0)	{
			showError("You have selected \"Other\" as the reason for your inquiry. Please include a brief message in the \"Additional Information\" box.", "errorAddInformation");
			}
		}
 //check a day of the week is chosen for contact
 
		function daysOfTheWeek()	{
				
			var mon = document.getElementById("Mon").checked;
			var tue = document.getElementById("Tue").checked;
			var wed = document.getElementById("Wed").checked;
			var thur = document.getElementById("Th").checked;
			var fri = document.getElementById("Fri").checked;
    
			if	(mon == false && tue == false && wed == false && thur == false && fri == false)	{
			showError("Please select a day of the week which is best for contact --we'd hate to bother you.", "errorDay");
			}
		}
 
 //the error message if any of these goes awry
 
		function showError(message, errorLocation)	{
			document.getElementById(errorLocation).innerHTML = message;
		}
//call functions

 nameValidate();
 emailPhoneValidate();
 reasonForInquiry();
 daysOfTheWeek();
 }
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 